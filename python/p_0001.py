"""
Problem:        1
Title:          Multiples of 3 and 5
Author:         Paul Cheor Kim
Description:
    If we list all the natural numbers below 10 that are multiples of 3 or 5, 
    we get 3, 5, 6 and 9. The sum of these multiples is 23.

    Find the sum of all the multiples of 3 or 5 below 1000.
"""

import time

THREE = 3
FIVE = 5

current_time_milli = lambda: int(round(time.time() * 1000))

def Timer(func):
    def wrapper(N, *args, **kwargs):
        before = current_time_milli()
        result = func(N)
        elapsed = current_time_milli() - before
        print("\nfunction %s executed in %d ms." % (func.__name__, elapsed))
        return result
    return wrapper

def is_divisible(num, den):
    return num % den == 0

def get_set_of_multiples(num, den):
    multiples = set()
    i = den
    while i < num:
        multiples.add(i)
        i += den
    return multiples

@Timer
def solve_by_list_comprehension(N):
    """
    Solve using list comprehension.
    """
    divisibleByThreeOrFive = [ i for i in range(N) if is_divisible(i, THREE) or is_divisible(i, FIVE) ]
    sum = 0
    for i in divisibleByThreeOrFive:
        sum += i
    return sum

@Timer
def solve_by_iteration_if(N):
    """
    Solve using basic iterative techniques.
    """
    sum = 0
    for i in range(N):
        if is_divisible(i, THREE) or is_divisible(i, FIVE):
            sum += i
    return sum

@Timer
def solve_by_sets(N):
    """
    Determine multiples by summation.
    """
    mult_threes = get_set_of_multiples(N, THREE)
    mult_fives = get_set_of_multiples(N, FIVE)
    return sum(mult_threes.union(mult_fives))

def main():
    FORMAT = 'The sum of all natural numbers under %d that is at least a multiple or %d or %d is: %d.'
    N = 10
    print(FORMAT % (N, THREE, FIVE, solve_by_list_comprehension(N)))
    print(FORMAT % (N, THREE, FIVE, solve_by_iteration_if(N)))
    print(FORMAT % (N, THREE, FIVE, solve_by_sets(N)))

    N = 1000
    print(FORMAT % (N, THREE, FIVE, solve_by_list_comprehension(N)))
    print(FORMAT % (N, THREE, FIVE, solve_by_iteration_if(N)))
    print(FORMAT % (N, THREE, FIVE, solve_by_sets(N)))

main()
